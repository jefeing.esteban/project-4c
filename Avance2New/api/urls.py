from django.urls import path

from api import views

app_name = 'api'

urlpatterns = [
    #API CARRERAS
    path('v1/carrera/list/', views.CarrerasList.as_view(), name='carreras_list'),
    path('v1/carrera/create/', views.CarrerasCreate.as_view(), name='carreras_create'),
    path('v1/carrera/<int:pk>/', views.CarrerasDetail.as_view(), name='carreras_detail'),
    path('v1/carrera/<int:pk>/update/', views.CarrerasUpdate.as_view(), name='carreras_update'),
    path('v1/carrera/<int:pk>/delete/', views.CarrerasDelete.as_view(), name='carreras_delete'),


    # API GRUPOS
    path('v1/grupo/list/', views.GruposList.as_view(), name='grupos_list'),
    path('v1/grupo/create/', views.GruposCreate.as_view(), name='grupos_create'),
    path('v1/grupo/<int:pk>/', views.GruposDetail.as_view(), name='grupos_detail'),
    path('v1/grupo/<int:pk>/update/', views.GruposUpdate.as_view(), name='grupos_update'),
    path('v1/grupo/<int:pk>/delete/', views.GruposDelete.as_view(), name='grupos_delete'),


    #API CARRERAS_GRUPOS
    path('v1/carr_grups/list/', views.CarrGrupsList.as_view(), name='carr_grups_list'),
    path('v1/carr_grups/create/', views.CarrGrupsCreate.as_view(), name='carr_grups_create'),
    path('v1/carr_grups/<int:pk>/', views.CarrGrupsDetail.as_view(), name='carr_grups_detail'),
    path('v1/carr_grups/<int:pk>/update/', views.CarrGrupsUpdate.as_view(), name='carr_grups_update'),
    path('v1/carr_grups/<int:pk>/delete/', views.CarrGrupsDelete.as_view(), name='carr_grups_delete'),


    #API ALUMNOS
    path('v1/alumnos/list/', views.AlumnosList.as_view(), name='alumnos_list'),
    path('v1/alumnos/create/', views.AlumnosCreate.as_view(), name='alumnos_create'),
    path('v1/alumnos/<int:pk>/', views.AlumnosDetail.as_view(), name='alumnos_detail'),
    path('v1/alumnos/<int:pk>/update/', views.AlumnosUpdate.as_view(), name='alumnos_update'),
    path('v1/alumnos/<int:pk>/delete/', views.AlumnosDelete.as_view(), name='alumnos_delete'),


    #API PUESTOS
    path('v1/puestos/list/', views.PuestosList.as_view(), name='puestos_list'),
    path('v1/puestos/create/', views.PuestosCreate.as_view(), name='puestos_create'),
    path('v1/puestos/<int:pk>/', views.PuestosDetail.as_view(), name='puestos_detail'),
    path('v1/puestos/<int:pk>/update/', views.PuestosUpdate.as_view(), name='puestos_update'),
    path('v1/puestos/<int:pk>/delete/', views.PuestosDelete.as_view(), name='puestos_delete'),


    #API DEPARTAMENTOS
    path('v1/departamentos/list/', views.DepartamentosList.as_view(), name='departamentos_list'),
    path('v1/departamentos/create/', views.DepartamentosCreate.as_view(), name='departamentos_create'),
    path('v1/departamentos/<int:pk>/', views.DepartamentosDetail.as_view(), name='departamentos_detail'),
    path('v1/departamentos/<int:pk>/update/', views.DepartamentosUpdate.as_view(), name='departamentos_update'),
    path('v1/departamentos/<int:pk>/delete/', views.DepartamentosDelete.as_view(), name='departamentos_delete'),


    #API PUESTO_DEPTO
    path('v1/puesto_depto/list/', views.PuestoDeptoList.as_view(), name='puesto_depto_list'),
    path('v1/puesto_depto/create/', views.PuestoDeptoCreate.as_view(), name='puesto_depto_create'),
    path('v1/puesto_depto/<int:pk>/', views.PuestoDeptoDetail.as_view(), name='puesto_depto_detail'),
    path('v1/puesto_depto/<int:pk>/update/', views.PuestoDeptoUpdate.as_view(), name='puesto_depto_update'),
    path('v1/puesto_depto/<int:pk>/delete/', views.PuestoDeptoDelete.as_view(), name='puesto_depto_delete'),

    
    #API ADMINISTRATIVOS
    path('v1/administrativos/list/', views.AdministrativosList.as_view(), name='administrativos_list'),
    path('v1/administrativos/create/', views.AdministrativosCreate.as_view(), name='administrativos_create'),
    path('v1/administrativos/<int:pk>/', views.AdministrativosDetail.as_view(), name='administrativos_detail'),
    path('v1/administrativos/<int:pk>/update/', views.AdministrativosUpdate.as_view(), name='administrativos_update'),
    path('v1/administrativos/<int:pk>/delete/', views.AdministrativosDelete.as_view(), name='administrativos_delete'),

    #API DOCENTES
    path('v1/docentes/list/', views.DocentesList.as_view(), name='docentes_list'),
    path('v1/docentes/create/', views.DocentesCreate.as_view(), name='docentes_create'),
    path('v1/docentes/<int:pk>/', views.DocentesDetail.as_view(), name='docentes_detail'),
    path('v1/docentes/<int:pk>/update/', views.DocentesUpdate.as_view(), name='docentes_update'),
    path('v1/docentes/<int:pk>/delete/', views.DocentesDelete.as_view(), name='docentes_delete'),


    #API CARRERA_DOCENTE
    path('v1/carrera_docente/list/', views.CarreraDocenteList.as_view(), name='carrera_docente_list'),
    path('v1/carrera_docente/create/', views.CarreraDocenteCreate.as_view(), name='carrera_docente_create'),
    path('v1/carrera_docente/<int:pk>/', views.CarreraDocenteDetail.as_view(), name='carrera_docente_detail'),
    path('v1/carrera_docente/<int:pk>/update/', views.CarreraDocenteUpdate.as_view(), name='carrera_docente_update'),
    path('v1/carrera_docente/<int:pk>/delete/', views.CarreraDocenteDelete.as_view(), name='carrera_docente_delete'),


    #API GENEROS
    path('v1/generos/list/', views.GenerosList.as_view(), name='generos_list'),
    path('v1/generos/create/', views.GenerosCreate.as_view(), name='generos_create'),
    path('v1/generos/<int:pk>/', views.GenerosDetail.as_view(), name='generos_detail'),
    path('v1/generos/<int:pk>/update/', views.GenerosUpdate.as_view(), name='generos_update'),
    path('v1/generos/<int:pk>/delete/', views.GenerosDelete.as_view(), name='generos_delete'),


    #API ESTADOS
    path('v1/estados/list/', views.EstadosList.as_view(), name='estados_list'),
    path('v1/estados/create/', views.EstadosCreate.as_view(), name='estados_create'),
    path('v1/estados/<int:pk>/', views.EstadosDetail.as_view(), name='estados_detail'),
    path('v1/estados/<int:pk>/update/', views.EstadosUpdate.as_view(), name='estados_update'),
    path('v1/estados/<int:pk>/delete/', views.EstadosDelete.as_view(), name='estados_delete'),


    #API USUARIOS
    path('v1/usuarios/list/', views.UsuariosList.as_view(), name='usuarios_list'),
    path('v1/usuarios/create/', views.UsuariosCreate.as_view(), name='usuarios_create'),
    path('v1/usuarios/<int:pk>/', views.UsuariosDetail.as_view(), name='usuarios_detail'),
    path('v1/usuarios/<int:pk>/update/', views.UsuariosUpdate.as_view(), name='usuarios_update'),
    path('v1/usuarios/<int:pk>/delete/', views.UsuariosDelete.as_view(), name='usuarios_delete'),


    #API DOCENCIAS
    path('v1/docencias/list/', views.DocenciasList.as_view(), name='docencias_list'),
    path('v1/docencias/create/', views.DocenciasCreate.as_view(), name='docencias_create'),
    path('v1/docencias/<int:pk>/', views.DocenciasDetail.as_view(), name='docencias_detail'),
    path('v1/docencias/<int:pk>/update/', views.DocenciasUpdate.as_view(), name='docencias_update'),
    path('v1/docencias/<int:pk>/delete/', views.DocenciasDelete.as_view(), name='docencias_delete'),


    #API UBICACIONES
    path('v1/ubicaciones/list/', views.UbicacionesList.as_view(), name='ubicaciones_list'),
    path('v1/ubicaciones/create/', views.UbicacionesCreate.as_view(), name='ubicaciones_create'),
    path('v1/ubicaciones/<int:pk>/', views.UbicacionesDetail.as_view(), name='ubicaciones_detail'),
    path('v1/ubicaciones/<int:pk>/update/', views.UbicacionesUpdate.as_view(), name='ubicaciones_update'),
    path('v1/ubicaciones/<int:pk>/delete/', views.UbicacionesDelete.as_view(), name='ubicaciones_delete'),


    #API TAMANOS
    path('v1/tamanos/list/', views.TamanosList.as_view(), name='tamanos_list'),
    path('v1/tamanos/create/', views.TamanosCreate.as_view(), name='tamanos_create'),
    path('v1/tamanos/<int:pk>/', views.TamanosDetail.as_view(), name='tamanos_detail'),
    path('v1/tamanos/<int:pk>/update/', views.TamanosUpdate.as_view(), name='tamanos_update'),
    path('v1/tamanos/<int:pk>/delete/', views.TamanosDelete.as_view(), name='tamanos_delete'),


    #API DISPONIBILIDAD
    path('v1/disponibilidad/list/', views.DisponibilidadList.as_view(), name='disponibilidad_list'),
    path('v1/disponibilidad/create/', views.DisponibilidadCreate.as_view(), name='disponibilidad_create'),
    path('v1/disponibilidad/<int:pk>/', views.DisponibilidadDetail.as_view(), name='disponibilidad_detail'),
    path('v1/disponibilidad/<int:pk>/update/', views.DisponibilidadUpdate.as_view(), name='disponibilidad_update'),
    path('v1/disponibilidad/<int:pk>/delete/', views.DisponibilidadDelete.as_view(), name='disponibilidad_delete'),


    #API CASILLEROS
    path('v1/casilleros/list/', views.CasillerosList.as_view(), name='casilleros_list'),
    path('v1/casilleros/create/', views.CasillerosCreate.as_view(), name='casilleros_create'),
    path('v1/casilleros/<int:pk>/', views.CasillerosDetail.as_view(), name='casilleros_detail'),
    path('v1/casilleros/<int:pk>/update/', views.CasillerosUpdate.as_view(), name='casilleros_update'),
    path('v1/casilleros/<int:pk>/delete/', views.CasillerosDelete.as_view(), name='casilleros_delete'),

    #API PERIODOS
    path('v1/periodos/list/', views.PeriodosList.as_view(), name='periodos_list'),
    path('v1/periodos/create/', views.PeriodosCreate.as_view(), name='periodos_create'),
    path('v1/periodos/<int:pk>/', views.PeriodosDetail.as_view(), name='periodos_detail'),
    path('v1/periodos/<int:pk>/update/', views.PeriodosUpdate.as_view(), name='periodos_update'),
    path('v1/periodos/<int:pk>/delete/', views.PeriodosDelete.as_view(), name='periodos_delete'),


    #API RENTAS
    path('v1/rentas/list/', views.RentasList.as_view(), name='rentas_list'),
    path('v1/rentas/create/', views.RentasCreate.as_view(), name='rentas_create'),
    path('v1/rentas/<int:pk>/', views.RentasDetail.as_view(), name='rentas_detail'),
    path('v1/rentas/<int:pk>/update/', views.RentasUpdate.as_view(), name='rentas_update'),
    path('v1/rentas/<int:pk>/delete/', views.RentasDelete.as_view(), name='rentas_delete'),


    #API PAGOS
    path('v1/pagos/list/', views.PagosList.as_view(), name='pagos_list'),
    path('v1/pagos/create/', views.PagosCreate.as_view(), name='pagos_create'),
    path('v1/pagos/<int:pk>/', views.PagosDetail.as_view(), name='pagos_detail'),
    path('v1/pagos/<int:pk>/update/', views.PagosUpdate.as_view(), name='pagos_update'),
    path('v1/pagos/<int:pk>/delete/', views.PagosDelete.as_view(), name='pagos_delete'),

]

