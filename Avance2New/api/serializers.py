from rest_framework import serializers

from core.models import Carreras, Pagos, Rentas, Periodos, Casilleros, Disponibilidad, Ubicaciones, Docencias, Estados, Generos, Usuarios, Carrera_Docente, Docentes, Administrativos, Puesto_Depto, Departamentos, Puestos, Alumnos, Carr_Grups, Grupos, Tamanos

## MODEL CARRERAS
class CarrerasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Carreras
        fields = ('nombre',)

#MODEL GRUPOS
class GruposSerializer(serializers.ModelSerializer):
    class Meta:
        model = Grupos
        fields = '__all__'


#MODEL CARRERAS_GRUPOS
class CarrGrupsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Carr_Grups
        fields = '__all__'


#MODEL ALUMNOS
class AlumnosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alumnos
        fields = '__all__'


#MODEL PUESTOS
class PuestosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Puestos
        fields = '__all__'


#MODEL DEPARTAMENTOS
class DepartamentosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Departamentos
        fields = '__all__'


#MODEL PUESTO_DEPTO
class PuestoDeptoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Puesto_Depto
        fields = '__all__'


#MODEL ADMINISTRATIVOS
class AdministrativosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Administrativos
        fields = '__all__'


#MODEL DOCENTES
class DocentesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Docentes
        fields = '__all__'


#MODEL CARRERA_DOCENTES
class CarreraDocenteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Carrera_Docente
        fields = '__all__'


#MODEL GENEROS
class GenerosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Generos
        fields = '__all__'


#MODEL ESTADOS
class EstadosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estados
        fields = '__all__'


#MODEL USUARIOS
class UsuariosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuarios
        fields = '__all__'


#MODEL DOCENCIAS
class DocenciasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Docencias
        fields = '__all__'


#MODEL UBICACIONES
class UbicacionesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ubicaciones
        fields = '__all__'


#MODEL TAMANOS
class TamanosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tamanos
        fields = '__all__'


#MODEL DISPONIBILIDAD
class DisponibilidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Disponibilidad
        fields = '__all__'


#MODEL CASILLEROS
class CasillerosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Casilleros
        fields = '__all__'


# MODEL PERIODOS
class PeriodosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Periodos
        fields = '__all__'


#MODEL RENTAS
class RentasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rentas
        fields = '__all__'


# MODEL PAGOS
class PagosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pagos
        fields = '__all__'
