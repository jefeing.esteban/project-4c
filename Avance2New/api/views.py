from django.shortcuts import render


from rest_framework import generics
from core.models import *
from .serializers import *
# Create your views here.


## MODEL CARRERAS
class CarrerasList(generics.ListAPIView):
    queryset = Carreras.objects.all()
    serializer_class = CarrerasSerializer

class CarrerasCreate(generics.CreateAPIView):
    queryset = Carreras.objects.all()
    serializer_class = CarrerasSerializer

class CarrerasDetail(generics.RetrieveAPIView):
    queryset = Carreras.objects.all()
    serializer_class = CarrerasSerializer

class CarrerasUpdate(generics.UpdateAPIView):
    queryset = Carreras.objects.all()
    serializer_class = CarrerasSerializer

class CarrerasDelete(generics.DestroyAPIView):
    queryset = Carreras.objects.all()
    serializer_class = CarrerasSerializer


#MODEL GRUPOS
class GruposList(generics.ListCreateAPIView):
    queryset = Grupos.objects.all()
    serializer_class = GruposSerializer

class GruposCreate(generics.CreateAPIView):
    queryset = Grupos.objects.all()
    serializer_class = GruposSerializer

class GruposDetail(generics.RetrieveAPIView):
    queryset = Grupos.objects.all()
    serializer_class = GruposSerializer

class GruposUpdate(generics.UpdateAPIView):
    queryset = Grupos.objects.all()
    serializer_class = GruposSerializer

class GruposDelete(generics.DestroyAPIView):
    queryset = Grupos.objects.all()
    serializer_class = GruposSerializer


#MODEL CARRERAS_GRUPOS
class CarrGrupsList(generics.ListCreateAPIView):
    queryset = Carr_Grups.objects.all()
    serializer_class = CarrGrupsSerializer

class CarrGrupsCreate(generics.CreateAPIView):
    queryset = Carr_Grups.objects.all()
    serializer_class = CarrGrupsSerializer

class CarrGrupsDetail(generics.RetrieveAPIView):
    queryset = Carr_Grups.objects.all()
    serializer_class = CarrGrupsSerializer

class CarrGrupsUpdate(generics.UpdateAPIView):
    queryset = Carr_Grups.objects.all()
    serializer_class = CarrGrupsSerializer

class CarrGrupsDelete(generics.DestroyAPIView):
    queryset = Carr_Grups.objects.all()
    serializer_class = CarrGrupsSerializer


#MODEL ALUMNOS
class AlumnosList(generics.ListCreateAPIView):
    queryset = Alumnos.objects.all()
    serializer_class = AlumnosSerializer

class AlumnosCreate(generics.CreateAPIView):
    queryset = Alumnos.objects.all()
    serializer_class = AlumnosSerializer

class AlumnosDetail(generics.RetrieveAPIView):
    queryset = Alumnos.objects.all()
    serializer_class = AlumnosSerializer

class AlumnosUpdate(generics.UpdateAPIView):
    queryset = Alumnos.objects.all()
    serializer_class = AlumnosSerializer

class AlumnosDelete(generics.DestroyAPIView):
    queryset = Alumnos.objects.all()
    serializer_class = AlumnosSerializer


#MODEL PUESTOS
class PuestosList(generics.ListCreateAPIView):
    queryset = Puestos.objects.all()
    serializer_class = PuestosSerializer

class PuestosCreate(generics.CreateAPIView):
    queryset = Puestos.objects.all()
    serializer_class = PuestosSerializer

class PuestosDetail(generics.RetrieveAPIView):
    queryset = Puestos.objects.all()
    serializer_class = PuestosSerializer

class PuestosUpdate(generics.UpdateAPIView):
    queryset = Puestos.objects.all()
    serializer_class = PuestosSerializer

class PuestosDelete(generics.DestroyAPIView):
    queryset = Puestos.objects.all()
    serializer_class = PuestosSerializer


#MODEL DEPARTAMENTOS
class DepartamentosList(generics.ListCreateAPIView):
    queryset = Departamentos.objects.all()
    serializer_class = DepartamentosSerializer

class DepartamentosCreate(generics.CreateAPIView):
    queryset = Departamentos.objects.all()
    serializer_class = DepartamentosSerializer

class DepartamentosDetail(generics.RetrieveAPIView):
    queryset = Departamentos.objects.all()
    serializer_class = DepartamentosSerializer

class DepartamentosUpdate(generics.UpdateAPIView):
    queryset = Departamentos.objects.all()
    serializer_class = DepartamentosSerializer

class DepartamentosDelete(generics.DestroyAPIView):
    queryset = Departamentos.objects.all()
    serializer_class = DepartamentosSerializer


#MODEL PUESTO_DEPTO
class PuestoDeptoList(generics.ListCreateAPIView):
    queryset = Puesto_Depto.objects.all()
    serializer_class = PuestoDeptoSerializer

class PuestoDeptoCreate(generics.CreateAPIView):
    queryset = Puesto_Depto.objects.all()
    serializer_class = PuestoDeptoSerializer

class PuestoDeptoDetail(generics.RetrieveAPIView):
    queryset = Puesto_Depto.objects.all()
    serializer_class = PuestoDeptoSerializer

class PuestoDeptoUpdate(generics.UpdateAPIView):
    queryset = Puesto_Depto.objects.all()
    serializer_class = PuestoDeptoSerializer

class PuestoDeptoDelete(generics.DestroyAPIView):
    queryset = Puesto_Depto.objects.all()
    serializer_class = PuestoDeptoSerializer


#MODEL ADMINISTRATIVOS
class AdministrativosList(generics.ListCreateAPIView):
    queryset = Administrativos.objects.all()
    serializer_class = AdministrativosSerializer

class AdministrativosCreate(generics.CreateAPIView):
    queryset = Administrativos.objects.all()
    serializer_class = AdministrativosSerializer

class AdministrativosDetail(generics.RetrieveAPIView):
    queryset = Administrativos.objects.all()
    serializer_class = AdministrativosSerializer

class AdministrativosUpdate(generics.UpdateAPIView):
    queryset = Administrativos.objects.all()
    serializer_class = AdministrativosSerializer

class AdministrativosDelete(generics.DestroyAPIView):
    queryset = Administrativos.objects.all()
    serializer_class = AdministrativosSerializer


#MODEL DOCENTES
class DocentesList(generics.ListCreateAPIView):
    queryset = Docentes.objects.all()
    serializer_class = DocentesSerializer

class DocentesCreate(generics.CreateAPIView):
    queryset = Docentes.objects.all()
    serializer_class = DocentesSerializer

class DocentesDetail(generics.RetrieveAPIView):
    queryset = Docentes.objects.all()
    serializer_class = DocentesSerializer

class DocentesUpdate(generics.UpdateAPIView):
    queryset = Docentes.objects.all()
    serializer_class = DocentesSerializer

class DocentesDelete(generics.DestroyAPIView):
    queryset = Docentes.objects.all()
    serializer_class = DocentesSerializer


#MODEL CARRERA
class CarreraDocenteList(generics.ListCreateAPIView):
    queryset = Carrera_Docente.objects.all()
    serializer_class = CarreraDocenteSerializer

class CarreraDocenteCreate(generics.CreateAPIView):
    queryset = Carrera_Docente.objects.all()
    serializer_class = CarreraDocenteSerializer

class CarreraDocenteDetail(generics.RetrieveAPIView):
    queryset = Carrera_Docente.objects.all()
    serializer_class = CarreraDocenteSerializer

class CarreraDocenteUpdate(generics.UpdateAPIView):
    queryset = Carrera_Docente.objects.all()
    serializer_class = CarreraDocenteSerializer

class CarreraDocenteDelete(generics.DestroyAPIView):
    queryset = Carrera_Docente.objects.all()
    serializer_class = CarreraDocenteSerializer


#MODEL GENEROS
class GenerosList(generics.ListCreateAPIView):
    queryset = Generos.objects.all()
    serializer_class = GenerosSerializer

class GenerosCreate(generics.CreateAPIView):
    queryset = Generos.objects.all()
    serializer_class = GenerosSerializer

class GenerosDetail(generics.RetrieveAPIView):
    queryset = Generos.objects.all()
    serializer_class = GenerosSerializer

class GenerosUpdate(generics.UpdateAPIView):
    queryset = Generos.objects.all()
    serializer_class = GenerosSerializer

class GenerosDelete(generics.DestroyAPIView):
    queryset = Generos.objects.all()
    serializer_class = GenerosSerializer


#MODEL ESTADOS
class EstadosList(generics.ListCreateAPIView):
    queryset = Estados.objects.all()
    serializer_class = EstadosSerializer

class EstadosCreate(generics.CreateAPIView):
    queryset = Estados.objects.all()
    serializer_class = EstadosSerializer

class EstadosDetail(generics.RetrieveAPIView):
    queryset = Estados.objects.all()
    serializer_class = EstadosSerializer

class EstadosUpdate(generics.UpdateAPIView):
    queryset = Estados.objects.all()
    serializer_class = EstadosSerializer

class EstadosDelete(generics.DestroyAPIView):
    queryset = Estados.objects.all()
    serializer_class = EstadosSerializer


#MODEL USUARIOS
class UsuariosList(generics.ListCreateAPIView):
    queryset = Usuarios.objects.all()
    serializer_class = UsuariosSerializer

class UsuariosCreate(generics.CreateAPIView):
    queryset = Usuarios.objects.all()
    serializer_class = UsuariosSerializer

class UsuariosDetail(generics.RetrieveAPIView):
    queryset = Usuarios.objects.all()
    serializer_class = UsuariosSerializer

class UsuariosUpdate(generics.UpdateAPIView):
    queryset = Usuarios.objects.all()
    serializer_class = UsuariosSerializer

class UsuariosDelete(generics.DestroyAPIView):
    queryset = Usuarios.objects.all()
    serializer_class = UsuariosSerializer


#MODEL DOCENCIAS
class DocenciasList(generics.ListCreateAPIView):
    queryset = Docencias.objects.all()
    serializer_class = DocenciasSerializer

class DocenciasCreate(generics.CreateAPIView):
    queryset = Docencias.objects.all()
    serializer_class = DocenciasSerializer

class DocenciasDetail(generics.RetrieveAPIView):
    queryset = Docencias.objects.all()
    serializer_class = DocenciasSerializer

class DocenciasUpdate(generics.UpdateAPIView):
    queryset = Docencias.objects.all()
    serializer_class = DocenciasSerializer

class DocenciasDelete(generics.DestroyAPIView):
    queryset = Docencias.objects.all()
    serializer_class = DocenciasSerializer


#MODEL UBICACIONES
class UbicacionesList(generics.ListCreateAPIView):
    queryset = Ubicaciones.objects.all()
    serializer_class = UbicacionesSerializer

class UbicacionesCreate(generics.CreateAPIView):
    queryset = Ubicaciones.objects.all()
    serializer_class = UbicacionesSerializer

class UbicacionesDetail(generics.RetrieveAPIView):
    queryset = Ubicaciones.objects.all()
    serializer_class = UbicacionesSerializer

class UbicacionesUpdate(generics.UpdateAPIView):
    queryset = Ubicaciones.objects.all()
    serializer_class = UbicacionesSerializer

class UbicacionesDelete(generics.DestroyAPIView):
    queryset = Ubicaciones.objects.all()
    serializer_class = UbicacionesSerializer


#MODEL TAMANOS
class TamanosList(generics.ListCreateAPIView):
    queryset = Tamanos.objects.all()
    serializer_class = TamanosSerializer

class TamanosCreate(generics.CreateAPIView):
    queryset = Tamanos.objects.all()
    serializer_class = TamanosSerializer

class TamanosDetail(generics.RetrieveAPIView):
    queryset = Tamanos.objects.all()
    serializer_class = TamanosSerializer

class TamanosUpdate(generics.UpdateAPIView):
    queryset = Tamanos.objects.all()
    serializer_class = TamanosSerializer

class TamanosDelete(generics.DestroyAPIView):
    queryset = Tamanos.objects.all()
    serializer_class = TamanosSerializer


#MODEL DISPONIBILIDAD
class DisponibilidadList(generics.ListCreateAPIView):
    queryset = Disponibilidad.objects.all()
    serializer_class = DisponibilidadSerializer

class DisponibilidadCreate(generics.CreateAPIView):
    queryset = Disponibilidad.objects.all()
    serializer_class = DisponibilidadSerializer

class DisponibilidadDetail(generics.RetrieveAPIView):
    queryset = Disponibilidad.objects.all()
    serializer_class = DisponibilidadSerializer

class DisponibilidadUpdate(generics.UpdateAPIView):
    queryset = Disponibilidad.objects.all()
    serializer_class = DisponibilidadSerializer

class DisponibilidadDelete(generics.DestroyAPIView):
    queryset = Disponibilidad.objects.all()
    serializer_class = DisponibilidadSerializer


#MODEL CASILLEROS
class CasillerosList(generics.ListCreateAPIView):
    queryset = Casilleros.objects.all()
    serializer_class = CasillerosSerializer

class CasillerosCreate(generics.CreateAPIView):
    queryset = Casilleros.objects.all()
    serializer_class = CasillerosSerializer

class CasillerosDetail(generics.RetrieveAPIView):
    queryset = Casilleros.objects.all()
    serializer_class = CasillerosSerializer

class CasillerosUpdate(generics.UpdateAPIView):
    queryset = Casilleros.objects.all()
    serializer_class = CasillerosSerializer

class CasillerosDelete(generics.DestroyAPIView):
    queryset = Casilleros.objects.all()
    serializer_class = CasillerosSerializer


#MODEL PERIODOS
class PeriodosList(generics.ListCreateAPIView):
    queryset = Periodos.objects.all()
    serializer_class = PeriodosSerializer

class PeriodosCreate(generics.CreateAPIView):
    queryset = Periodos.objects.all()
    serializer_class = PeriodosSerializer

class PeriodosDetail(generics.RetrieveAPIView):
    queryset = Periodos.objects.all()
    serializer_class = PeriodosSerializer

class PeriodosUpdate(generics.UpdateAPIView):
    queryset = Periodos.objects.all()
    serializer_class = PeriodosSerializer

class PeriodosDelete(generics.DestroyAPIView):
    queryset = Periodos.objects.all()
    serializer_class = PeriodosSerializer


#MODEL RENTAS
class RentasList(generics.ListCreateAPIView):
    queryset = Rentas.objects.all()
    serializer_class = RentasSerializer

class RentasCreate(generics.CreateAPIView):
    queryset = Rentas.objects.all()
    serializer_class = RentasSerializer

class RentasDetail(generics.RetrieveAPIView):
    queryset = Rentas.objects.all()
    serializer_class = RentasSerializer

class RentasUpdate(generics.UpdateAPIView):
    queryset = Rentas.objects.all()
    serializer_class = RentasSerializer

class RentasDelete(generics.DestroyAPIView):
    queryset = Rentas.objects.all()
    serializer_class = RentasSerializer


#MODEL PAGOS
class PagosList(generics.ListCreateAPIView):
    queryset = Pagos.objects.all()
    serializer_class = PagosSerializer

class PagosCreate(generics.CreateAPIView):
    queryset = Pagos.objects.all()
    serializer_class = PagosSerializer

class PagosDetail(generics.RetrieveAPIView):
    queryset = Pagos.objects.all()
    serializer_class = PagosSerializer

class PagosUpdate(generics.UpdateAPIView):
    queryset = Pagos.objects.all()
    serializer_class = PagosSerializer

class PagosDelete(generics.DestroyAPIView):
    queryset = Pagos.objects.all()
    serializer_class = PagosSerializer
