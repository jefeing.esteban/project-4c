from django.db import models

# Create your models here.

## MODEL CARRERAS
class Carreras(models.Model):
    codigo = models.CharField(max_length=4, default="0")
    nombre = models.CharField(max_length=64, null=False)

    def __str__(self):
        return self.nombre


## MODEL GRUPOS
class Grupos(models.Model):
    codigo = models.CharField(max_length=2, default="0")
    grado = models.IntegerField(default=0)
    grupo = models.CharField(max_length=2, default="0")

    def __str__(self):
        return f"{self.grado} y {self.grupo}"


## MODEL CARRERA-GRUPOS
class Carr_Grups(models.Model):
    carrera = models.ForeignKey(Carreras, on_delete=models.CASCADE)
    grupo = models.ManyToManyField(Grupos)

    def __str__(self):
        return "{} / {}".format(self.carrera, ', '.join(self.grupo.all().values_list('grupo', flat=True)))


## MODEL ALUMNOS
class Alumnos(models.Model):
    matricula = models.CharField(default="0", max_length=4)
    grupo = models.ForeignKey(Grupos, on_delete=models.CASCADE)
    carrera = models.ForeignKey(Carreras, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.grupo} y {self.carrera}"


## MODEL PUESTOS
class Puestos(models.Model):
    codigo = models.CharField(default="0", max_length=4)
    nombre = models.CharField(default="Admin", max_length=64)
    #Departamentos = models.ManyToManyField('Departamento', through='PUESTO_DEPTO')

    def __str__(self):
        return self.nombre


## MODEL DEPARTAMENTO
class Departamentos(models.Model):
    codigo = models.CharField(max_length=4, default="0")
    nombre = models.CharField(default="Contabilidad", max_length=64)
    #Puestos = models.ManyToManyField('Puesto', through='PUESTO_DEPTO')

    def __str__(self):
        return self.nombre


## MODEL PUESTO-DEPTO
class Puesto_Depto(models.Model):
    puesto = models.ForeignKey(Puestos, on_delete=models.CASCADE)
    departamento = models.ManyToManyField(Departamentos)

    def __str__(self):
        return "{} / {}".format(self.departamento, ', '.join(self.puesto.all().values_list('puesto', flat=True)))


## MODEL ADMINISTRATIVOS
class Administrativos(models.Model):
    numero = models.IntegerField()
    puesto = models.ForeignKey(Puestos, on_delete=models.CASCADE)
    departamento = models.ForeignKey(Departamentos, on_delete=models.CASCADE)

    def __str__(self):
        return self.numero


## MODEL DOCENTES
class Docentes(models.Model):
    numero = models.IntegerField()
    carrera = models.ForeignKey(Carreras, on_delete=models.CASCADE)

    def __str__(self):
        return self.numero


## MODEL CARRERA-DOCENTES
class Carrera_Docente(models.Model):
    docente = models.ForeignKey(Docentes, on_delete=models.CASCADE)
    carrera = models.ManyToManyField(Carreras)

    def __str__(self):
        return "{} / {}".format(self.docente, ', '.join(self.carrera.all().values_list('carrera', flat=True)))
    

## MODEL GENEROS
class Generos(models.Model):
    codigo = models.CharField(default="0", max_length=4)
    genero = models.CharField(default="Masculino", max_length=32)

    def __str__(self):
        return self.genero


## MODEL ESTADOS
class Estados(models.Model):
    codigo = models.CharField(default="0", max_length=4)
    estado = models.CharField(default="Steve", max_length=32)

    def __str__(self):
        return self.estado


## MODEL USUARIOS
class Usuarios(models.Model):
    numero = models.IntegerField()
    #HuellaDigital = models.BinaryField()
    nombrePila = models.CharField(default="Steve", max_length=128)
    apPaterno = models.CharField(default="Olmos", max_length=64)
    apMaterno = models.CharField(default="Labastida", max_length=64)
    genero = models.ForeignKey(Generos, on_delete=models.CASCADE)
    fechaNacimiento = models.DateTimeField(auto_now_add=True, auto_now=False)
    numTel = models.CharField(default="666-666-96-98", max_length=16)
    email = models.CharField(default="@gmail.com", max_length=128)
    calle = models.CharField(default="Calle X", max_length=128)
    colonia = models.CharField(default="Refugio", max_length=128)
    numExterior = models.CharField(default="1234", max_length=4)
    alumno = models.ForeignKey(Alumnos, on_delete=models.CASCADE, null=False)
    docente = models.ForeignKey(Docentes, on_delete=models.CASCADE, null=False)
    administrativo = models.ForeignKey(Administrativos, on_delete=models.CASCADE)
    estadoActual = models.ForeignKey(Estados, on_delete=models.CASCADE)
    fechaRegistro = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return f"{self.nombrePila} {self.apPaterno} {self.apMaterno}"
    

## MODEL DOCENCIAS
class Docencias(models.Model):
    codigo = models.CharField(default="0", max_length=4)
    nombre = models.CharField(default="Steve", max_length=64)

    def __str__(self):
        return self.nombre
    

## MODEL UBICACIONES
class Ubicaciones(models.Model):
    codigo = models.CharField(default="0", max_length=4)
    piso = models.CharField(default="Steve", max_length=1)
    seccion = models.CharField(default="Steve", max_length=1)
    docencia = models.ForeignKey(Docencias, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return f"{self.piso} y {self.seccion}"


## MODEL TAMAÑOS
class Tamanos(models.Model):
    codigo = models.CharField(default="0", max_length=4)
    descripcion = models.CharField(max_length=64, null=False)
    dimensiones = models.CharField(max_length=16, null=False)
    precio = models.FloatField(blank=True, null=True)

    def __str__(self):
        return self.descripcion


## MODEL DISPONIBILIDAD
class Disponibilidad(models.Model):
    codigo = models.CharField(default="0", max_length=4)
    descripcion = models.CharField(max_length=64, null=False)

    def __str__(self):
        return self.descripcion


## MODEL CASILLEROS
class Casilleros(models.Model):
    numero = models.IntegerField()
    disponibilidad = models.ForeignKey(Disponibilidad, on_delete=models.CASCADE, null=False)
    ubicacion = models.ForeignKey(Ubicaciones, on_delete=models.CASCADE, null=False)
    tamano = models.ForeignKey(Tamanos, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return str(self.numero)
    

## MODEL PERIODOS
class Periodos(models.Model):
    folio = models.CharField(default="0", max_length=8)
    meses = models.CharField(max_length=32, null=False)

    def __str__(self):
        return self.folio
    

## MODEL RENTAS
class Rentas(models.Model):
    referencia = models.CharField(default="0", max_length=16)
    fechaInicio = models.DateTimeField(auto_now_add=True, auto_now=False)
    fechaFinal = models.DateTimeField(auto_now_add=True, auto_now=False)
    usuario = models.ForeignKey(Usuarios, on_delete=models.CASCADE, null=False)
    casillero = models.ForeignKey(Casilleros, on_delete=models.CASCADE, null=False)
    periodo = models.ForeignKey(Periodos, on_delete=models.CASCADE, null=False)
    estado = models.ForeignKey(Estados, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return self.referencia


## MODEL PAGOS
class Pagos(models.Model):
    folio = models.IntegerField()
    fechaPago = models.DateTimeField(auto_now_add=True, auto_now=False)
    total = models.FloatField(blank=True, null=True)
    renta = models.ForeignKey(Rentas, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return str(self.folio)
