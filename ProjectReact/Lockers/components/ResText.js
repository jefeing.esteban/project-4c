import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import React, { useState,useEffect } from 'react';



export default function ResText() {
  const [carreras, setCarreras] = useState(null);

  useEffect(function(){
    async function fetchCarreras() {
      //const response = await fetch("http://localhost:8000/api/v1/carrera/list/");
      //response = await response.json()
      fetch("http://localhost:8000/api/v1/carrera/list/")
        .then(response => response.json())
        .then(response => {
          console.log(response)
          for (let index = 0; index < response.length; index++) {
            setCarreras(response[index].nombre);
            
          }
          //setCarreras(response[0].nombre)
          
        })
   
    }
    fetchCarreras();
  }, [])

  return (
    <View style={styles.container}>
      <Text>Carreras {carreras}</Text>
      <StatusBar style="auto" />
    </View>
  );
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });